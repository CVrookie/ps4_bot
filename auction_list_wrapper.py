#!/usr/bin/python3

import time
import sys
import subprocess
import cv2
import ps4BotUtils
from ps4BotUtils import Ps4BotUtils
from random import randint

WAITTIMELOADSCREEN=0.5
WAITTIMELISTAUCTION=0.5
MATCHCONFIDENCESHAPE = 0.99
MATCHCONFIDENCEPLAYEREDGE = 0.45
MATCHCONFIDENCEPLAYERSHAPE = 0.98
MATCHCONFIDENCEAUCTIONCREATED = 0.97
MATCHCONFIDENCESHAPEEDGE = 0.70
MATCHCONFIDENCECOLOR = 0.85
PRICESILVER = 600
PRICEGOLD = 700
PRICEKEMBALOW  = 7200
PRICEKEMBAHIGH = 8150
PRICEWESTBROOKLOW  = 8500
PRICEWESTBROOKHIGH = 9750
PRICELILLARDLOW  = 8200
PRICELILLARDHIGH = 9500
PRICEJOKICLOW  = 7200
PRICEJOKICHIGH = 8000
PRICEZIONLOW  = 8900
PRICEZIONHIGH = 10200
PRICESTOCKTONLOW  = 8400
PRICESTOCKTONHIGH = 9400
PRICEMIDDLETONLOW  = 8900
PRICEMIDDLETONHIGH = 9600
PRICEMUGSYLOW  = 6250
PRICEMUGSYHIGH = 10000


def create_random_price(low,high):
    temp = randint(low, high)
    temp = 50*round(temp/50)
    print(temp)
    return(temp)

def create_auction(tPs4, price):
    numRightPresses = int((price-250)/50)
    templateAuctionCreated= cv2.imread("template_auctionCreated.png")

    

    tPs4.press_release('Return')
    time.sleep(WAITTIMELOADSCREEN)
    tPs4.press_release('Return')
    time.sleep(WAITTIMELOADSCREEN)
    tPs4.press_release('Down')
    for _ in range(numRightPresses):
        tPs4.press_release('Right')
    tPs4.press_release('Down')
    tPs4.press_release('Right')
    tPs4.press_release('Return')
    tPs4.press_release('Down')  
    # input()  
    tPs4.press_release('Return')
    while(not tPs4.check_for_match(templateAuctionCreated,MATCHCONFIDENCEAUCTIONCREATED)):
        time.sleep(WAITTIMELISTAUCTION)
    tPs4.press_release('Return')
    time.sleep(WAITTIMELISTAUCTION)
      


if __name__ == "__main__":
    if len(sys.argv) !=2:
        print("incorrect length")
        exit(1)

    try:
        numIters = int(sys.argv[1])
    except:
        print("arg2 is not a number")
        exit(3)

    Ps4 = Ps4BotUtils()   
    templateGold= cv2.imread("template_gold.png")
    templateSilver= cv2.imread("template_silver.png")
    templateSilverGames= cv2.imread("template_silverCardGames.png")
    templateGoldGames= cv2.imread("template_goldCardGames.png")

    template_activeAuctionMask = cv2.imread("template_activeAuctionMask.png")
    templateKemba= cv2.imread("template_kemba.png")
    templateWestbrook= cv2.imread("template_westbrook.png")
    templateJokic= cv2.imread("template_jokic.png")
    templateLillard= cv2.imread("template_lillard.png")
    templateZion= cv2.imread("template_zion.png")
    templateStockton= cv2.imread("template_stockton.png")
    templateMiddleton= cv2.imread("template_middleton.png")
    templateMugsy= cv2.imread("template_mugsy.png")

    for i in range(numIters):
        time.sleep(1)
        screenCap = Ps4.get_screenshot()
        if Ps4.check_for_match(templateGold, MATCHCONFIDENCESHAPE, screenCap=screenCap) and Ps4.check_for_match(templateGoldGames, MATCHCONFIDENCESHAPEEDGE, matchType=1,screenCap=screenCap):
            create_auction(Ps4, PRICEGOLD)            
        elif Ps4.check_for_match(templateSilver, MATCHCONFIDENCESHAPE, screenCap=screenCap) and Ps4.check_for_match(templateSilverGames, MATCHCONFIDENCESHAPEEDGE, matchType=1,screenCap=screenCap):
            create_auction(Ps4,PRICESILVER)
        elif Ps4.check_for_match(templateKemba, MATCHCONFIDENCEPLAYERSHAPE, screenCap=screenCap, learn=True) and Ps4.check_for_match(templateKemba, MATCHCONFIDENCEPLAYEREDGE, matchType=1,screenCap=screenCap, learn=True):
            create_auction(Ps4, create_random_price(PRICEKEMBALOW,PRICEKEMBAHIGH))
        elif Ps4.check_for_match(templateZion, MATCHCONFIDENCEPLAYERSHAPE, screenCap=screenCap) and Ps4.check_for_match(templateZion, MATCHCONFIDENCEPLAYEREDGE, matchType=1,screenCap=screenCap):
            create_auction(Ps4, create_random_price(PRICEZIONLOW,PRICEZIONHIGH))
        elif Ps4.check_for_match(templateLillard, MATCHCONFIDENCEPLAYERSHAPE, screenCap=screenCap) and Ps4.check_for_match(templateLillard, MATCHCONFIDENCEPLAYEREDGE, matchType=1,screenCap=screenCap):
            create_auction(Ps4, create_random_price(PRICELILLARDLOW,PRICELILLARDHIGH))
        elif Ps4.check_for_match(templateJokic, MATCHCONFIDENCEPLAYERSHAPE, screenCap=screenCap) and Ps4.check_for_match(templateJokic, MATCHCONFIDENCEPLAYEREDGE, matchType=1,screenCap=screenCap):
            create_auction(Ps4, create_random_price(PRICEJOKICLOW,PRICEJOKICHIGH))
        elif Ps4.check_for_match(templateWestbrook, MATCHCONFIDENCEPLAYERSHAPE, screenCap=screenCap) and Ps4.check_for_match(templateWestbrook, MATCHCONFIDENCEPLAYEREDGE, matchType=1,screenCap=screenCap):
            create_auction(Ps4, create_random_price(PRICEWESTBROOKLOW,PRICEWESTBROOKHIGH))
        elif Ps4.check_for_match(templateStockton, MATCHCONFIDENCEPLAYERSHAPE, screenCap=screenCap) and Ps4.check_for_match(templateStockton, MATCHCONFIDENCEPLAYEREDGE, matchType=1,screenCap=screenCap):
            create_auction(Ps4, create_random_price(PRICESTOCKTONLOW,PRICESTOCKTONHIGH))
        elif Ps4.check_for_match(templateMiddleton, MATCHCONFIDENCEPLAYERSHAPE, screenCap=screenCap) and Ps4.check_for_match(templateMiddleton, MATCHCONFIDENCEPLAYEREDGE, matchType=1,screenCap=screenCap):
            create_auction(Ps4, create_random_price(PRICEMIDDLETONLOW,PRICEMIDDLETONHIGH))
        elif Ps4.check_for_match(templateMugsy, MATCHCONFIDENCEPLAYERSHAPE, screenCap=screenCap) and Ps4.check_for_match(templateMugsy, MATCHCONFIDENCEPLAYEREDGE, matchType=1,screenCap=screenCap):
            create_auction(Ps4, create_random_price(PRICEMUGSYLOW,PRICEMUGSYHIGH))
        else:
            print('None')
            Ps4.press_release('Left')
