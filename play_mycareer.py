#!/usr/bin/python3

import time
import sys
import subprocess
import cv2
from ps4BotUtils import Ps4BotUtils

WAITCOLLECTIONPAGE=1


if __name__ == "__main__":

    Ps4 = Ps4BotUtils()
    template= cv2.imread("template_myCareerNextGame.png")
    templatePractice= cv2.imread("template_myCareerGoToTeamPractice.png")
    
    while True:
        time.sleep(1)
        if (Ps4.check_for_match(template,0.99)):
            Ps4.press_release("Down")
        elif (Ps4.check_for_match(templatePractice,0.99)):
            Ps4.press_release("Right")
        time.sleep(3)
        Ps4.press_release('Return')
        # Ps4.press_release('BackSpace')