#!/usr/bin/python3

import time
import sys
import subprocess
import cv2
from ps4BotUtils import Ps4BotUtils
import os



TIMERACTIONSTART = 0
TIMERACTIONSTOP  = 1

#p1 926, 294 129 38
#p2 904, 393

#p1 190,136
#p2 190,198 & 61,27
BIDPOINT = (136,190)
BUYITNOWPOINT = (198, 190)
PRICESIZE = (27,61)
TIMEAUCTIONCARDACTIVEDELAY = 0.8
TIMESCREENSHOTDELAY = 1
PRINTINTERVALS = 10
returnCode = 1
filename = 'test2_{}.png'

flip = True
startTime=None
endTime=None
runningTime = [0, 0, 0]

# class subTimers:
#     def __init__(self, id, action):
#         self.id = id
#         if action
class timers:
    def __init__(self):
        self.nTimers = 0

def calculate_time():
    global runningTime
    global startTime
    global endTime

    temp = endTime - startTime
    runningTime[0] = (runningTime[0]*runningTime[1] + temp)/(runningTime[1]+1)
    runningTime[1] = runningTime[1] + 1
    runningTime[2] = runningTime[2] + temp
    
    if not runningTime[1] % PRINTINTERVALS:
        sinceInterval = runningTime[2]/PRINTINTERVALS
        runningTime[2] = 0
        print ("Total Average search takes:    {}".format(runningTime[0]))
        print ("Interval Average search takes: {}".format(sinceInterval))
        print ("Number of Iterations:          {}\n".format(runningTime[1]))
    
def makeCanny(img):
    imgG = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) 
    imgB = cv2.blur(imgG,(2,2)) 
    imgC =  cv2.Canny(imgB, 250,255) 
    return(imgC)

def bid_auction(ps4Temp):
    ps4Temp.press_release('Return')
    ps4Temp.press_release('Return')
    ps4Temp.press_release('Down')
    ps4Temp.press_release('Return')

def buy_auction(ps4Temp):
    ps4Temp.press_release('Return')
    time.sleep(TIMEAUCTIONCARDACTIVEDELAY)
    ps4Temp.press_release('Down')
    ps4Temp.press_release('Return')
    ps4Temp.press_release('Down')
    ps4Temp.press_release('Return')

def research(ps4Temp, numDown, numRight, takeScreenshot=None):
    global flip
    global startTime
    global endTime

    if takeScreenshot:
        time.sleep(TIMESCREENSHOTDELAY)
        tempImg=ps4Temp.get_screenshot()
        cv2.imwrite(filename.format(time.time()),tempImg)
    
    ps4Temp.press_release('BackSpace')
    
    for _ in range(numDown):
        ps4Temp.press_release('Down')
    
    time.sleep(0.05)
    if flip == True:
        flip = False
        for _ in range(numRight):    
            ps4Temp.press_release('Left')
    else:
        flip = True
        for _ in range(numRight):    
            ps4Temp.press_release('Right')
    if (startTime and endTime):
        calculate_time()
    
    startTime = time.time()
    ps4Temp.press_release('o')
    


    
if __name__ == "__main__":
    # global startTime
    # global endTime

    if len(sys.argv) !=3:
        print("incorrect length")
        exit(1)

    try:
        numDown = int(sys.argv[1])
        numRight = int(sys.argv[2])
    except:
        print("Both arguments need to be numbers")
        exit(3)

    Ps4 = Ps4BotUtils()
    templateSuccess= cv2.imread("template_inAuction.png")
    templateSuccessBoughtOut= cv2.imread("template_successBoughtOut.png")
    templateFailBoughtOut= cv2.imread("template_boughtOutAuction.png")
    templateFail= cv2.imread("template_searchFail.png")
    templateBidSuccess= cv2.imread("template_bidSuccess1.png")

    while True:
        screen = Ps4.get_screenshot()
        if (Ps4.check_for_match(templateSuccess,0.9,screenCap=screen, matchType=1)):
            endTime = time.time()
            tempVar = time.time()
            cv2.imwrite(filename.format(time.time()),screen)
            print("imagewrite {}".format(time.time()-tempVar))

            imgBid =screen[BIDPOINT[0]:BIDPOINT[0]+PRICESIZE[0], BIDPOINT[1]:BIDPOINT[1]+PRICESIZE[1]] 
            imgBuy = screen[BUYITNOWPOINT[0]:BUYITNOWPOINT[0]+PRICESIZE[0], BUYITNOWPOINT[1]:BUYITNOWPOINT[1]+PRICESIZE[1]]
            imgBuyCanny = makeCanny(imgBuy)
            imgBidCanny = makeCanny(imgBid)
            r = cv2.matchTemplate(imgBuyCanny, imgBidCanny, cv2.TM_CCORR_NORMED)
            _,match,_,_ = cv2.minMaxLoc(r)
            if (match > 0.84):
                bid_auction(Ps4)
            else:
                buy_auction(Ps4)
            tempVar = time.time()
            os.system('aplay ping.wav&')
            print("imagewrite {}".format(time.time()-tempVar))

            time.sleep(0.5)
            screen = Ps4.get_screenshot()
            while True:
                if Ps4.check_for_match(templateBidSuccess, 0.9,matchType=0,screenCap=screen):
                    Ps4.press_release('BackSpace')
                    buy_auction(Ps4)
                screen = Ps4.get_screenshot()
                if Ps4.check_for_match(templateFailBoughtOut, 0.8,matchType=1,screenCap=screen):
                    Ps4.press_release('BackSpace')
                    Ps4.press_release('BackSpace')
                    research(Ps4, numDown, numRight,True)
                    break
                elif Ps4.check_for_match(templateSuccessBoughtOut, 0.8,matchType=1,screenCap=screen):
                    os.system('aplay success.wav&')
                    Ps4.press_release('BackSpace')
                    research(Ps4, numDown, numRight, True)
                    break
                else:
                    print ("FAIL")
                    continue

        elif (Ps4.check_for_match(templateFail,0.9,screenCap=screen)):
            endTime = time.time()
            research(Ps4, numDown, numRight)
        
#p1 192,138 &55,22
#p2 192,200

#p1 190,136
#p2 190,198 & 61,27

#p1 926, 294 129 38
#p2 904, 393