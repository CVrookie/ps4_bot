#!/usr/bin/python3

import time
import sys
import subprocess
import cv2
from ps4BotUtils import Ps4BotUtils

WAITOUTCOMESPAGE =0.2


if __name__ == "__main__":

    if len(sys.argv) !=2:
        print("incorrect length")
        exit(1)

    try:
        numIters = int(sys.argv[1])
    except:
        print("arg2 is not a number")
        exit(3)
    returnValue = 1
    goldAdded = 0
    silverAdded = 0
    templateSilverCardStack = cv2.imread("template_silverCardStack.png")
    templateGoldCardStack = cv2.imread("template_goldCardStack.png")
    
    Ps4 = Ps4BotUtils()
    
    for i in range(numIters):
        screenCap = Ps4.get_screenshot()

        if (goldAdded == 1 and silverAdded == 1):
            break
        if(goldAdded == 0 and Ps4.check_for_match(templateGoldCardStack,0.89, 0.85,1,screenCap=screenCap)):
            Ps4.press_release("Return")
            Ps4.press_release("Up")
            Ps4.press_release("Up")
            Ps4.press_release("Return")
            goldAdded = 1
            returnValue=0
        
        elif(silverAdded == 0 and Ps4.check_for_match(templateSilverCardStack,0.89, 0.85,1,screenCap=screenCap)):
            Ps4.press_release("Return")
            Ps4.press_release("Up")
            Ps4.press_release("Up")
            Ps4.press_release("Return")
            silverAdded = 1
            returnValue=0
        
        Ps4.press_release('Down')
        time.sleep(WAITOUTCOMESPAGE)
    exit(returnValue)
    