#!/usr/bin/python3

import time
import sys
import subprocess
import cv2
from ps4BotUtils import Ps4BotUtils

WAITOUTCOMESPAGE =0.5


if __name__ == "__main__":

    Ps4 = Ps4BotUtils()
    template= cv2.imread("template_myteamQuit.png")
    templateOutcomesFull= cv2.imread("template_outcomesFull.png")

    while(not Ps4.check_for_match(template,0.95, learn=True)):
        Ps4.press_release('BackSpace')
    Ps4.press_release("Return")

    for i in range(7):
        Ps4.press_release('3')
    for i in range(2):
        Ps4.press_release('2')
    Ps4.press_release('Down')
    Ps4.press_release('Return')
    time.sleep(WAITOUTCOMESPAGE)
    if Ps4.check_for_match(templateOutcomesFull, 0.99):
        Ps4.press_release('Return')
        