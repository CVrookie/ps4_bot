#!/usr/bin/python3

import time
import sys
import subprocess
import cv2
from ps4BotUtils import Ps4BotUtils

returnCode = 1


if __name__ == "__main__":

    Ps4 = Ps4BotUtils()
    template= cv2.imread("template_amethystSearch.png")

    Ps4.press_release('Down')
    Ps4.press_release('Down')
    for _ in range(4):
        Ps4.press_release('Left')
    for _ in range(6):
        Ps4.press_release('Down')
    for _ in range(9):
        Ps4.press_release('Right')

    if Ps4.check_for_match(template, 0.97,matchType=1):
        Ps4.press_release('o')
        returnCode = 0

    exit(returnCode)
