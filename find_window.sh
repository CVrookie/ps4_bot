#!/bin/bash


for i in `xprop -root|grep "_NET_CLIENT_LIST_STACKING(WINDOW): window id" |tr '#' ','|tr ',' '\n'| grep 0x`
do 
    temp=`xwininfo -id $i|grep "Window id.*$1"| grep -o 0x[0-9a-f]*`
    # echo $temp
    if [ ! -z "$temp" ]
    then
        echo $temp
        exit 0
    fi  
done
exit 1
