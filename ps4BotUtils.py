#!/usr/bin/python3

import time
import sys
import subprocess
import cv2
import os

tempImgName = "/tmpRam/.ps4bot_temp_screencap.png"
ps4WinNameDefault = "Chiaki | Stream"
AFTERPRESSDELAYDEFAULT = 0.05
winCmd='./find_window.sh'
capCmd='import -silent -window {win} {tempImgName}'
defaultCmd = './keypress.sh  {win} {button}'

def get_window(winName):
    window = subprocess.check_output([winCmd,'"'+winName+'"'])
    window = window.split()[0]
    window = int(window,16)
    return window


def makeCanny(img):
    imgG = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) 
    imgB = cv2.blur(imgG,(2,2)) 
    imgC =  cv2.Canny(imgB, 250,255) 
    return(imgC)


class Ps4BotUtils():
    def __init__(self,ps4WinName=ps4WinNameDefault,afterPressDelay=AFTERPRESSDELAYDEFAULT):
        self.windowId = get_window(ps4WinName)
        self.AFTERPRESSDELAY = afterPressDelay
        self.__lastPressTime__ = 0

    def press_release(self, keyin):
        timeElapsed = time.time() - self.__lastPressTime__
        if timeElapsed < self.AFTERPRESSDELAY:
            time.sleep(self.AFTERPRESSDELAY-timeElapsed)
        subprocess.call(defaultCmd.format(win = self.windowId, button=keyin).split())
        self.__lastPressTime__=time.time()    

    def get_screenshot(self):
        subprocess.call(capCmd.format(win=self.windowId, tempImgName=tempImgName).split())
        return(cv2.imread(tempImgName))
        os.remove(tempImgName)

    def check_for_match(self, template, matchMin,matchMinHSV=0, matchType = 0, screenCap = None, learn=False):
        h_bins = 25
        s_bins = 30
        histSize = [h_bins, s_bins]
        h_ranges = [0, 180]
        s_ranges = [0, 256]
        ranges = h_ranges + s_ranges
        channels = [0, 1]

        if type(screenCap) == type(None):
            screenCap = self.get_screenshot()            
        screenCapMatch = screenCap
        if matchType == 1:
            screenCapMatch = makeCanny(screenCap)
                
        templateMatch = template
        templateHSV = cv2.cvtColor(template,cv2.COLOR_BGR2HSV)
        if matchType == 1:
            templateMatch = makeCanny(template)
        templateHist = cv2.calcHist([templateHSV], channels, None, histSize, ranges, accumulate=False)
        cv2.normalize(templateHist, templateHist, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX)
        
        
        matcher=cv2.matchTemplate(screenCapMatch,templateMatch,cv2.TM_CCORR_NORMED)
        error,match,_,matchLoc=cv2.minMaxLoc(matcher)

        screenCapCrop = screenCap[matchLoc[1]:matchLoc[1]+template.shape[0],matchLoc[0]:matchLoc[0]+template.shape[1]]
        screenCapHSV = cv2.cvtColor(screenCapCrop,cv2.COLOR_BGR2HSV)
        screenCapHist = cv2.calcHist([screenCapHSV], channels, None, histSize, ranges, accumulate=False)
        cv2.normalize(screenCapHist, screenCapHist, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX)


        histCorrelatoin = cv2.compareHist(screenCapHist, templateHist, 0)
        if learn: 
            print (histCorrelatoin)
            print(error,match)
    
        if match > matchMin and histCorrelatoin > matchMinHSV:
            return True
        else:
            return False


