#!/usr/bin/python3

import time
import sys
import subprocess
from ps4BotUtils import Ps4BotUtils
import cv2

auctionFull = 1
searchEmpty = 0
WAITAUCTIONSEND = 2
WAITPOPUPLOAD = 1
if __name__ == "__main__":
    
    if len(sys.argv) != 1:
        print("incorrect length")
        exit(1)
    
    returnVal = None
    templateAcutionFull = cv2.imread("template_auctionOutOfSpace.png")
    templateSearchEmpty = cv2.imread("template_emptySearch.png")
    
    Ps4 = Ps4BotUtils()
    while True:
        Ps4.press_release('Return')
        Ps4.press_release('Return')
        time.sleep(WAITAUCTIONSEND)
        screenCap = Ps4.get_screenshot()
        if Ps4.check_for_match(templateSearchEmpty, 0.999, screenCap=screenCap):
            returnVal = searchEmpty
            break 
        elif Ps4.check_for_match(templateAcutionFull, 0.9,screenCap=screenCap):
            returnVal= auctionFull 
            Ps4.press_release('Return')           
            break
        
    exit (returnVal)




