#!/usr/bin/python3

import time
import sys
import subprocess
import cv2
from ps4BotUtils import Ps4BotUtils

WAITTIMEPOPUP = 0.3
MATCHCONFIDENCECARDEDGE = 0.93
MATCHCONFIDENCECARDCOLOR = 0.7
MATCHCONFIDENCECARDCOLOR = 0.7
MATCHCONFIDENCESOLD = 0.95
MATCHCONFIDENCEOUTBIDCOLOR = 0.75
MATCHCONFIDENCEOUTBID = 0.85
RETURNVALAUCTIONFULL = 1
RETURNVALUESUCCESS=0

if __name__ == "__main__":
    if len(sys.argv) !=2:
        print("incorrect length")
        exit(1)

    try:
        numIters = int(sys.argv[1])
    except:
        print("arg2 is not a number")
        exit(3)

    returnValue = RETURNVALUESUCCESS
    refillActive = True

    templateSold= cv2.imread("template_sold.png")
    templateOutbid= cv2.imread("template_outbid.png")
    templateGold= cv2.imread("template_goldCardOutcomes.png")
    templateSilver= cv2.imread("template_silverCardOutcomes.png")
    templateOutOfSpace = cv2.imread("template_auctionOutOfSpace.png")
    
    Ps4 = Ps4BotUtils()

    for i in range(numIters):
        time.sleep(1)
        
        screenCap = Ps4.get_screenshot()

        if Ps4.check_for_match(templateSold, MATCHCONFIDENCESOLD, screenCap=screenCap, learn=True) and \
        not Ps4.check_for_match(templateOutbid, MATCHCONFIDENCEOUTBID, MATCHCONFIDENCEOUTBIDCOLOR, matchType=1, screenCap=screenCap, learn=True):
            Ps4.press_release('Return')
            Ps4.press_release('Down')
            Ps4.press_release('Return')
            time.sleep(2)
        elif refillActive and \
        (
        Ps4.check_for_match(templateGold,MATCHCONFIDENCECARDEDGE,MATCHCONFIDENCECARDCOLOR,matchType=1, screenCap=screenCap, learn=True) or \
        Ps4.check_for_match(templateSilver,MATCHCONFIDENCECARDEDGE,MATCHCONFIDENCECARDCOLOR,matchType=1, screenCap=screenCap, learn=True)
        ):
            Ps4.press_release('Return')
            Ps4.press_release('Up')
            Ps4.press_release('Up')
            Ps4.press_release('Down')
            Ps4.press_release('Return')
            time.sleep(WAITTIMEPOPUP)
            if(Ps4.check_for_match(templateOutOfSpace, 0.99)):
                Ps4.press_release('Return')
                Ps4.press_release('BackSpace')
                refillActive = False
                returnValue = RETURNVALAUCTIONFULL
                
        else:
            Ps4.press_release('Down')

    exit(returnValue)