#!/usr/bin/python3

import time
import sys
import subprocess
import cv2
from ps4BotUtils import Ps4BotUtils

returnCode = 1


if __name__ == "__main__":

    Ps4 = Ps4BotUtils()
    template= cv2.imread("template_jordanSnipe.png")

    Ps4.press_release('Down')
    Ps4.press_release('Down')
    Ps4.press_release('Down')
    for _ in range(23):
        Ps4.press_release('Right')
    for _ in range(6):
        Ps4.press_release('Down')
    Ps4.press_release('Right')
    time.sleep(1)
    if Ps4.check_for_match(template, 0.99,matchType=0):
        Ps4.press_release('o')
