#!/usr/bin/python3

import time
import sys
import subprocess
import cv2
from ps4BotUtils import Ps4BotUtils

returnCode = 1


if __name__ == "__main__":
    if len(sys.argv) !=2:
        print("incorrect length")
        exit(1)

    try:
        numIters = int(sys.argv[1])
    except:
        print("arg2 is not a number")
        exit(3)


    Ps4 = Ps4BotUtils(afterPressDelay=0.5)
    templateBid1000= cv2.imread("template_bid1000.png")
    templateBidSuccess= cv2.imread("template_bidSuccess1.png")
    templateBidFailure= cv2.imread("template_bidFailure1.png")

    for _ in range(numIters):

        Ps4.press_release('Return')
        time.sleep(2)
        while not (Ps4.check_for_match(templateBid1000, 0.9, matchType=1)):            
            Ps4.press_release('Right')
            time.sleep(0.1)

        Ps4.press_release('Return')
        Ps4.press_release('Down')
        Ps4.press_release('Return')
        # Ps4.press_release('Return')

        while True:
            time.sleep(1)
            screen = Ps4.get_screenshot()
            if Ps4.check_for_match(templateBidSuccess, 0.9, matchType=0, screenCap=screen):
                Ps4.press_release('Return')
                print('success')
                break
            elif Ps4.check_for_match(templateBidFailure, 0.7, matchType=0, screenCap=screen):
                Ps4.press_release('Return')
                Ps4.press_release('BackSpace')
                print('fail')
                break
        time.sleep(2)   
        Ps4.press_release('Right')
