#!/bin/bash
refilled=0
addToAuctionResult=0

acceptMtArg=0
refillContractsArg=0
auctionListWrapperArg=0

if [ "$#" -ge 1 ] && [ $1 -eq $1 ] 2>/dev/null
then
    acceptMtArg=$1
fi

if [ "$#" -ge 2 ] && [ $2 -eq $2 ] 2>/dev/null
then
    refillContractsArg=$2
fi

if [ "$#" -ge 3 ] && [ $3 -eq $3 ] 2>/dev/null
then
    auctionListWrapperArg=$3
fi

python3.6 go_to_auction_outcomes.py
python3.6 accept_mt.py $acceptMtArg > /dev/null
addToAuctionResult=$?

if [ $addToAuctionResult == 1 ]
then
    refilled=1
fi 

while [ $refilled == 0 ]
do
    echo "going to contracts"
    python3.6 go_to_contracts.py > /dev/null
    validContracts=$?
    echo $validContracts
    if [ $validContracts == 0 ]
    then
        echo "in contracts"
        python3.6 send_to_auction.py > /dev/null
        addToAuctionResult=$?
    else
        echo "going to outcomes for refill"
        python3.6 go_to_auction_outcomes.py > /dev/null
        python3.6 refill_contracts.py $refillContractsArg > /dev/null
        refilled=$?
    fi

    if [ $addToAuctionResult == 1 ]
    then    
        echo "auction full"
        break
    fi
    sleep 5
done

echo "listing auction"

python3.6 go_to_active_auctions.py > /dev/null
sleep 1
python3.6 auction_list_wrapper.py $auctionListWrapperArg > /dev/null