#!/usr/bin/python3

import time
import sys
import subprocess
import cv2
from ps4BotUtils import Ps4BotUtils

WAITCOLLECTIONPAGE=0.5
WAITSEARCHLOAD = 5
RETURNVALSUCCESS = 0
RETURNVALFAILURE = 1

if __name__ == "__main__":

    returnVal = RETURNVALFAILURE
    template= cv2.imread("template_myteamQuit.png")
    templateSilverText= cv2.imread("template_silverText.png")
    templateGoldText= cv2.imread("template_goldText.png")
    returnVal = 1

    Ps4 = Ps4BotUtils()

    while(not Ps4.check_for_match(template,0.90,0.85)):
        Ps4.press_release('BackSpace')
    Ps4.press_release("Return")

    for i in range(7):
        Ps4.press_release('3')
    Ps4.press_release('2')
    Ps4.press_release('Right')
    Ps4.press_release('Return')
    time.sleep(WAITCOLLECTIONPAGE)
    Ps4.press_release('Return')
    for i in range(3):
        Ps4.press_release('Left')
    Ps4.press_release('Down')
    for i in range(5):
        
        time.sleep(0.5)
        screenCap = Ps4.get_screenshot()

        if ( Ps4.check_for_match(templateGoldText,0.999,0,screenCap=screenCap)):        
            Ps4.press_release("o")
            returnVal = RETURNVALSUCCESS
            break
    
        if ( Ps4.check_for_match(templateSilverText,0.999,0,screenCap=screenCap)):        
            Ps4.press_release("o")
            returnVal = RETURNVALSUCCESS
            break
    
        Ps4.press_release('Left')
        
    if returnVal == RETURNVALSUCCESS:
        time.sleep(WAITSEARCHLOAD)

    exit(returnVal)